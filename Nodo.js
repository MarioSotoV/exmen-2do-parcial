class Nodo{
constructor(nombre,nivel,padre,Hl,HR) {
    var aux=0;
    this.nombre=nombre;
    this.nivel=nivel;
    this.padre=padre;
    this.HL=Hl;
    this.HR=HR;
    if(Hl==16)
        this.HL=this.crearHijo(16,nivel+1,padre+1,1,1);
    if(HR==20)
        this.HR=this.crearHijo(20,nivel+1,padre+1,11,11)

    if(Hl==1)
        this.HL=this.crearHijo(8,nivel+1,padre+1,2,2);
    if(HR==1)
        this.HR=this.crearHijo(8,nivel+1,padre+1,3,3);

    if(Hl==2)
        this.HL=this.crearHijo("e",nivel+1,padre+1,0,0);
    if(HR==2)
        this.HR=this.crearHijo(4,nivel+1,padre+1,4,4);

    if(Hl==3)
        this.HL = this.crearHijo("a", nivel + 1, padre + 1, 0, 0);

    if(HR==3)
        this.HR=this.crearHijo(4,nivel+1,padre+1,5,5);

    if(Hl==4)
        this.HL = this.crearHijo("n", nivel + 1, padre + 1, 0, 0);
    if(HR==4)
        this.HR=this.crearHijo(2,nivel+1,padre+1,6,6);

    if(Hl==5)
        this.HL = this.crearHijo("t", nivel + 1, padre + 1, 0, 0);
    if(HR==5)
        this.HR=this.crearHijo("m",nivel+1,padre+1,0,0);

    if(Hl==6)
        this.HL = this.crearHijo("o", nivel + 1, padre + 1, 0, 0);
    if(HR==6)
        this.HR=this.crearHijo("u",nivel+1,padre+1,0,0);

    if(Hl==11)
        this.HL = this.crearHijo(8, nivel + 1, padre + 1, 12, 12);
    if(HR==11)
        this.HR=this.crearHijo(12,nivel+1,padre+1,13,13);

    if(Hl==12)
        this.HL = this.crearHijo(4, nivel + 1, padre + 1, 14, 14);
    if(HR==12)
        this.HR=this.crearHijo(4,nivel+1,padre+1,15,15);

    if(Hl==13)
        this.HL = this.crearHijo(5, nivel + 1, padre + 1, 160, 160);
    if(HR==13)
        this.HR=this.crearHijo(" ",nivel+1,padre+1,0,0);

    if(Hl==14)
        this.HL = this.crearHijo("i", nivel + 1, padre + 1, 0, 0);
    if(HR==14)
        this.HR=this.crearHijo(2,nivel+1,padre+1,18,18);

    if(Hl==15)
        this.HL = this.crearHijo("h", nivel + 1, padre + 1, 0, 0);
    if(HR==15)
        this.HR=this.crearHijo("s",nivel+1,padre+1,0,0);

    if(Hl==160)
        this.HL = this.crearHijo(2, nivel + 1, padre + 1, 17, 17);
    if(HR==160)
        this.HR=this.crearHijo("f",nivel+1,padre+1,0,0);

    if(Hl==17)
        this.HL = this.crearHijo("r", nivel + 1, padre + 1, 0, 0);
    if(HR==17)
        this.HR=this.crearHijo("l",nivel+1,padre+1,0,0);

    if(Hl==18)
        this.HL = this.crearHijo("x", nivel + 1, padre + 1, 0, 0);
    if(HR==18)
        this.HR=this.crearHijo("p",nivel+1,padre+1,0,0);

}

crearHijo(nombre,nivel,padre,HL,HR){
    var crearHijo=new Nodo(nombre,nivel,padre,HL,HR);
    return crearHijo;
}
}